'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class FormData extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Form.hasOne(models.FormData);
      models.FormData.belongsTo(models.Form);
    }
  }
  FormData.init({
    fixed_assets: DataTypes.JSON,
    current_assets: DataTypes.JSON,
    capital_and_reserves: DataTypes.JSON,
    long_obligations: DataTypes.JSON,
    short_obligations: DataTypes.JSON,
    for_quarter: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'FormData',
  });
  return FormData;
};