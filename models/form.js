'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Form extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Organization.hasOne(models.Form);
      models.Form.belongsTo(models.Organization);
    }
  }
  Form.init({
    is_have_auditor: DataTypes.BOOLEAN,
    auditor_name: DataTypes.STRING,
    auditor_tax_number: DataTypes.INTEGER,
    auditor_reg_number: DataTypes.INTEGER,
    OrganizationId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Form',
  });
  return Form;
};