'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('FormData', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fixed_assets: {
        type: Sequelize.JSON,
        allowNull: false
      },
      current_assets: {
        type: Sequelize.JSON,
        allowNull: false
      },
      capital_and_reserves: {
        type: Sequelize.JSON,
        allowNull: false
      },
      long_obligations: {
        type: Sequelize.JSON,
        allowNull: false
      },
      short_obligations: {
        type: Sequelize.JSON,
        allowNull: false
      },
      FormId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "Forms",
          key: "id"
        }
      },
      for_quarter: {
        type: Sequelize.DATE,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('FormData');
  }
};