const request = require("supertest");
const app = require("../app");

var form_id = 0;

describe("POST /forms", function() {
  it("Respond with success and id", async function() {
    const result = await request(app).post("/forms").send({
      is_have_auditor: true,
      auditor_name: "Test auditor",
      auditor_tax_number: 123456789,
      auditor_reg_number: 987654321,
      OrganizationId: 1,
    });

    expect(result.body.status).toEqual("success");
    form_id = result.body.form_id;
  });
});

describe("GET /forms/form_id", function() {
  it("Respond with our form", async function() {
    const result = await request(app).get("/forms/" + form_id);
    expect(result.body.auditor_name).toEqual("Test auditor");
    expect(result.body.auditor_reg_number).toEqual(987654321);
  });
});

describe("DELETE /forms/", function() {
  it("Respond with success", async function() {
    const result = await request(app).delete("/forms/" + form_id);
    expect(result.body.status).toEqual("success");
  });
});