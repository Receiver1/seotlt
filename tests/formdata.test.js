const request = require("supertest");
const app = require("../app");

var formdata_id = 0;

describe("POST /formsdata", function() {
  it("Respond with success and id", async function() {
    const result = await request(app).post("/formsdata").send({
      fixed_assets: {test: "test"},
      current_assets: {},
      capital_and_reserves: {},
      long_obligations: {},
      short_obligations: {},
      FormId: 1,
      for_quarter: new Date(),
    });

    expect(result.body.status).toEqual("success");
    formdata_id = result.body.formdata_id;
  });
});

describe("GET /formsdata/formdata_id", function() {
  it("Respond with our form data", async function() {
    const result = await request(app).get("/formsdata/" + formdata_id);
    expect(result.body.id).toEqual(formdata_id);
    expect(result.body.fixed_assets).toEqual({test: "test"});
  });
});

describe("DELETE /formsdata/", function() {
  it("Respond with success", async function() {
    const result = await request(app).delete("/formsdata/" + formdata_id);
    expect(result.body.status).toEqual("success");
  });
});