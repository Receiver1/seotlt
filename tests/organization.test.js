const request = require("supertest");
const app = require("../app");

var organization_id = 0;

describe("POST /organizations", function() {
  it("Respond with success and id", async function() {
    const result = await request(app).post("/organizations").send({
      name: "Test company",
      tax_number: 123456789,
      activity_type: "Test type",
      ownership_form: "Test form",
      location: "Test location"
    });

    expect(result.body.status).toEqual("success");
    organization_id = result.body.organization_id;
  });
});

describe("GET /organizations/organization_id", function() {
  it("Respond with our organization", async function() {
    const result = await request(app).get("/organizations/" + organization_id);
    expect(result.body.name).toEqual("Test company");
    expect(result.body.tax_number).toEqual(123456789);
  });
});

describe("DELETE /organizations/", function() {
  it("Respond with success", async function() {
    const result = await request(app).delete("/organizations/" + organization_id);
    expect(result.body.status).toEqual("success");
  });
});