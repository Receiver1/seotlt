## Запуск тестов
npm run test

## Запуск миграций
npm run migrate

## Отмена миграций
npm run migrate_off

## Запуск в дебаге
nodemon
