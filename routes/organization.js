var express = require('express');
const db = require("../models/index");
const build_options = require("./build_options");

var router = express.Router();

// Get organization list
router.get("/", async function(req, res, next) {
  try {
    const options = build_options(req);
    const organization = await db.Organization.findAll(options);
    res.send(organization);
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Search organization by id
router.get("/:id", async function(req, res, next) {
  try {
    const options = build_options(req);
    options.where = {id: req.params.id};
    const organization = await db.Organization.findOne(options);
    res.send(organization ?? {
      status: "error",
      message: "Organization not found",
    });
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Create new organization
router.post("/", async function(req, res, next) {
  try {
    const organization = await db.Organization.create(req.body);
    if (!organization) throw Error("Organization not created");
    res.json({
      status: "success",
      organization_id: organization.id,
    });
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Save organization by id
router.post("/:id", async function(req, res, next) {
  try {
    const organization = await db.Organization.findOne({where: {id: req.params.id}});
    if (!organization) throw new Error("Organization not created");
    Object.assign(organization, req.body);
    organization.save();
    res.json({
      status: "success",
      organization_id: organization.id,
    });
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Remove organization by id
router.delete("/:id", async function(req, res, next) {
  try {
    const organization = await db.Organization.findOne({where: {id: req.params.id}});
    if (!organization) throw Error("Organization not found")
    await db.Organization.destroy({where: {id: req.params.id}, truncate: true});
    res.json({status: "success"});
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

module.exports = router;