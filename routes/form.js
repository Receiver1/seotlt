const express = require('express');
const db = require("../models/index");
const build_options = require("./build_options");

var router = express.Router();

// Get form list
router.get("/", async function(req, res, next) {
  try {
    const options = build_options(req);
    const organization_id = req.query.OrganizationId;
    if (organization_id) {
      options.where = {OrganizationId: organization_id};
    }

    const forms = await db.Form.findAll(options);
    res.send(forms);
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Search form by id
router.get("/:id", async function(req, res, next) {
  try {
    const options = build_options(req);
    options.where = {id: req.params.id};
    const form = await db.Form.findOne(options);
    res.send(form ?? {
      status: "error",
      message: "Form not found",
    });
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Create new form
router.post("/", async function(req, res, next) {
  try {
    const form = await db.Form.create(req.body);
    if (!form) throw Error("Form not created");
    res.json({
      status: "success",
      form_id: form.id,
    });
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Save form by id
router.post("/:id", async function(req, res, next) {
  try {
    const form = await db.Form.findOne({where: {id: req.params.id}});
    if (!form) throw new Error("Form not created");
    Object.assign(form, req.body);
    form.save();
    res.json({
      status: "success",
      form_id: form.id,
    });
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Remove form by id
router.delete("/:id", async function(req, res, next) {
  try {
    const form = await db.Form.findOne({where: {id: req.params.id}});
    if (!form) throw Error("User not found")
    await db.Form.destroy({where: {id: req.params.id}, truncate: true});
    res.json({status: "success"});
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

module.exports = router;