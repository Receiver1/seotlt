function build_options(req) {
  let options = {};

  const filter = req.query.filter;
  if (filter) {
    options.attributes = filter.split(",");
  }

  const sort = req.query.sort;
  if (sort) {
    const desc = req.query.desc;
    options.order = [[sort, desc ? "DESC" : "ASC"]];
  }

  const limit = req.query.limit;
  if (limit) {
      options.limit = limit;
  }

  const offset = req.query.offset;
  if (offset) {
    options.offset = offset;
  }

  return options;
}

module.exports = build_options;