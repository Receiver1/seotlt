var express = require('express');
const db = require("../models/index");
const build_options = require("./build_options");

var router = express.Router();

// Get form data list
router.get("/", async function(req, res, next) {
  try {
    const options = build_options(req);
    const form_id = req.query.FormId;
    if (form_id) {
      options.where = {FormId: form_id};
    }

    const forms = await db.FormData.findAll(options);
    res.send(forms);
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Search form data by id
router.get("/:id", async function(req, res, next) {
  try {
    const options = build_options(req);
    options.where = {id: req.params.id};
    const form = await db.FormData.findOne(options);
    res.send(form ?? {
      status: "error",
      message: "Form data not found",
    });
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Create new form
router.post("/", async function(req, res, next) {
  try {
    const form = await db.FormData.create(req.body);
    if (!form) throw Error("Form data not created");
    res.json({
      status: "success",
      formdata_id: form.id,
    });
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Save form data by id
router.post("/:id", async function(req, res, next) {
  try {
    const form = await db.FormData.findOne({where: {id: req.params.id}});
    if (!form) throw new Error("Form data not created");
    Object.assign(form, req.body);
    form.save();
    res.json({
      status: "success",
      formdata_id: form.id,
    });
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

// Remove form data by id
router.delete("/:id", async function(req, res, next) {
  try {
    const form = await db.FormData.findOne({where: {id: req.params.id}});
    if (!form) throw Error("FormData not found")
    await db.FormData.destroy({where: {id: req.params.id}, truncate: true});
    res.json({status: "success"});
  } catch (error) {
    res.send({
      status: "error",
      message: error.message,
    });
  }
});

module.exports = router;